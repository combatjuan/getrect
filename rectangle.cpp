#include <emscripten/bind.h>
#include "rectangle.hpp"
#include <iostream>

#include <sanitizer/lsan_interface.h>

using namespace emscripten;

Rectangle::Rectangle(int x, int y, int width, int height)
	: _x(x), _y(y), _width(width), _height(height) {
	std::cout << "Rectangle constructing." << std::endl;
}

Rectangle::~Rectangle() {
	std::cout << "Rectangle destructing." << std::endl;
}


int rectangleArea(int w, int h) {
	return w * h;
}

int Rectangle::x() {
	return _x;
}

int Rectangle::y() {
	return _y;
}

int Rectangle::width() {
	return _width;
}

int Rectangle::height() {
	return _height;
}

int Rectangle::area() {
	return rectangleArea(_width, _height);
}

bool Rectangle::containsPoint(int x, int y) {
	return x >= _x && x <= (_x + _width) && y >= _y && y <= _y + _height;
}

// You must compile in support for address sanitization
// -fsanitize=leak or -fsantize=address
// for this feature to be available
// If there are leaks, the program will segfault.
void detectLeaks() {
#if defined(__has_feature)
	std::cout << "Checking for memory leaks..." << std::endl;
	__lsan_do_leak_check();
#else
	std::cout << "Leak check: Unsupported" << std::endl;
#endif
}

EMSCRIPTEN_BINDINGS(my_module) {
	function("rectangleArea", &rectangleArea);
	function("detectLeaks", &detectLeaks);

	class_<Rectangle>("Rectangle")
		.constructor<int, int, int, int>()
		.function("x", &Rectangle::x)
		.function("y", &Rectangle::y)
		.function("width", &Rectangle::width)
		.function("height", &Rectangle::height)
		.function("area", &Rectangle::area)
		.function("containsPoint", &Rectangle::containsPoint);
}
