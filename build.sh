#!/usr/bin/env bash
SOURCES=rectangle.cpp
OUTPUT=rectangle.js
OPTIONS="-fsanitize=leak -sALLOW_MEMORY_GROWTH"

emcc -lembind $OPTIONS -o $OUTPUT $SOURCES
