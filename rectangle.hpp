class Rectangle {
public:
	Rectangle(int x, int y, int width, int height);
	virtual ~Rectangle();

	int x();
	int y();
	int width();
	int height();
	int area();
	bool containsPoint(int x, int y);

private:
	int _x;
	int _y;
	int _width;
	int _height;
};
