Intro
=====
This project is intended to do some basic experimentation with web assembly and C++ binding.

Really basic stuff.

Objectives
==========
I want to understand more viscerally than I get reading documentation:

[X] What the build process looks like for compiling C++ and including the generated WASM in a simple HTML/javascript environment

[X] What it looks like to call into C++ from javascript

[X] What it looks like to pass data from javascript to C++

[X] What it looks like to reference a C++ object in javascript

[X] What it looks like to have a C++ object method called into by javascript

[X] What ownership semantics could look like for objects owned in the WASM layer but referenced in the javascript layer

[X] Whether I can reasonably manage the lifetime of the C++ objects in the javascript layer

[ ] What kinds of data can be easily passed back and forth between javascript and python (e.g. string, ints, JS objects, reference/pointer/handle like things etc.)

[ ] Some practical experience with mutating data held in C++ via functions called from javascript

Ultimately, I want to investigate what it looks like to have a high performance C++ library that owns and manages complex objects that are referenced in the javascript side and presented on the UI, but really live in C++.

My hypothesis is that this is pretty standard stuff and well-trodden ground and can be accomplished even by someone not particularly familiar with WASM or web development in general.  I have had some reason to believe I'm overly optimistic.

Tally Ho!

:-)

Lessons Learned
===============
* We'll have a bad time if we try and directly write all the bindings and interface code by hand.  Raw old skool WASM doesn't have nice ways to pass data.  You pretty much get single integer types, floating types, and memory buffers.  This makes life hard.

* But of course using the primitive types, you can build nicer things and good news!  The world has done that.  nbind and embind seem to be the leaders in doing this for C++.  They help create much nicer bindings.

* Embind is quite nice.  https://emscripten.org/docs/porting/connecting_cpp_and_javascript/embind.html#

* Oh dear, I've read ahead on the embind documentation.  Memory management isn't as magical as I had hoped it was.  From their documentation:
> JavaScript only gained support for finalizers in ECMAScript 2021, or ECMA-262 Edition 12. The new API is called FinalizationRegistry and it still does not offer any guarantees that the provided finalization callback will be called. Embind uses this for cleanup if available, but only for smart pointers, and only as a last resort.
>
> Warning
>
> It is strongly recommended that JavaScript code explicitly deletes any C++ object handles it has received.

Setup
=====
You'll need standard Linux build stuffs (compiler, linker, etc.).  Install yourself build-essential or whatever your equivalent package(s) is(are).

You also need emscripten.  I installed it with pacman on Arch.  I found that emcc was not automatically added to my path so I made a symlink to it in ~/bin/emcc.

Building
========
To compile embind:
`emcc -lembind -o rect.cpp rect.js`

Running
=======
Modern browsers don't give the same latitude as they used to on localhost (for good security reasons).  As a result, you can't just have a pile of files in a directory and open them with 'file:///path/to/cool/files'.

Nope.  You're going to have to set up nginx or something.  I installed nginx, made a directory under my nginx config, and then made a script that "installs" my files into that directory and lets the http user own them.  Then I can use my browser and access http://localhost/name-of-my-project/index.html.

You can even have an autocommand in vim run your install script on save.

Version
=======
Current Version 0.7

Feature Roadmap
===============
0.1 A stupid webpage with a canvas and a rectangle on it.

0.2 The webpage loads WASM and displays some data from WASM

0.3 The WASM layer takes a value from javascript and returns a result

0.4 The WASM layer takes some input from javascript (x, y, width, height) and returns a handle to a Rectangle

0.5 The user is able to "draw" a rectangle on the canvas which is backed by a C++ Rectangle

0.6 The user is able to select a Rectangle

0.7 The user is able to delete a Rectangle

0.8 The user is able to mutate a Rectangle

0.9 Rectangles are managed by javascript handles

1.0 You can generally make, create, select, modify, combine Rectangles in the UI and the state is nicely maintained with a simple API.  Oh dear.  That seems like a bigger step than the rest.
