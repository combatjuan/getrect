const X = 200;
const Y = 300;
const WIDTH = 300;
const HEIGHT = 500;

var rectangles = [];
var selected_rectangle = null;

// Keep track of mouse state for drawing purposes
var is_mouse_down = false;
var draw_start_x = null;
var draw_start_y = null;

function initialize() {
	let canvas = document.getElementById("canvas");

	canvas.onmousedown = function(e) {
		let canvas = document.getElementById("canvas");
		const canvas_bounds = canvas.getBoundingClientRect();
		draw_start_x = e.x - canvas_bounds.left;
		draw_start_y = e.y - canvas_bounds.top;
		is_mouse_down = true;
	}

	canvas.onmouseup = function(e) {
		let canvas = document.getElementById("canvas");
		const canvas_bounds = canvas.getBoundingClientRect();
		const final_x = e.x - canvas_bounds.left;
		const final_y = e.y - canvas_bounds.top;
		const x = Math.min(final_x, draw_start_x);
		const y = Math.min(final_y, draw_start_y);
		const width = Math.max(final_x, draw_start_x) - x;
		const height = Math.max(final_y, draw_start_y) - y;
		is_mouse_down = false;

		// Stupid handler to treat click-like interactions
		// differently than drag-like interactions.
		if (width > 5 && height > 5) {
			const rectangle = new Module.Rectangle(x, y, width, height);
			rectangles.push(rectangle);
			console.log('The area of the new rectangle is: ' + rectangle.area());
			draw();
		}
	}

	canvas.onmousemove = function(e) {
		if (is_mouse_down) {
			drawTemporary(e);
		}
	}

	canvas.onclick = function(e) {
		console.log("Clicked at (" + e.x + ", " + e.y + ")");
		selectRectangle(e.x, e.y);
		console.log("Selected rectangle is: ", selected_rectangle);
		draw();
	}

	draw();
}

function selectRectangle(x, y) {
	let temp_selected_rectangle = null;
	// Search front (last created) to back
	for (let i = rectangles.length - 1; i >= 0; i--) {
		let r = rectangles[i];
		if (r.containsPoint(x, y)) {
			temp_selected_rectangle = i;
			if (temp_selected_rectangle == selected_rectangle) {
				console.log("The same rectangle has been selected again.  Deleting rectangle " + temp_selected_rectangle);
				// I don't understand JS arrays very well
				rectangles.splice(temp_selected_rectangle, 1);
				r.delete();
				temp_selected_rectangle = null;
			}
			break;
		}
	}
	// I have to assume that real JS developers don't use so much global state.
	// To JS professionals reading this, my apologies.
	selected_rectangle = temp_selected_rectangle;
	return selected_rectangle;
}

function drawTemporary(e) {
	draw();

	let canvas = document.getElementById("canvas");
	const ctx = canvas.getContext("2d");
	const canvas_bounds = canvas.getBoundingClientRect();

	const final_x = e.x - canvas_bounds.left;
	const final_y = e.y - canvas_bounds.top;
	const x = Math.min(final_x, draw_start_x);
	const y = Math.min(final_y, draw_start_y);
	const width = Math.max(final_x, draw_start_x) - x;
	const height = Math.max(final_y, draw_start_y) - y;
	ctx.strokeRect(x, y, width, height);
}

function draw() {
	let canvas = document.getElementById("canvas");

	if (canvas.getContext) {
		const ctx = canvas.getContext("2d");
		ctx.clearRect(0, 0, canvas.width, canvas.height);

		for (let i = 0; i < rectangles.length; i++) {
			const r = rectangles[i];
			if (i == selected_rectangle) {
				ctx.fillStyle = "rgb(255, 0, 0)"
			} else {
				ctx.fillStyle = "rgb(0, 0, 200)"
			}
			ctx.fillRect(r.x(), r.y(), r.width(), r.height());
			ctx.strokeRect(r.x(), r.y(), r.width(), r.height());
		}
	}
}

var Module = {
	onRuntimeInitialized: function() {
		initialize();
		draw();
	}
};
